from django.db import models
from django.conf import settings


class User(models.Model):
    username = models.CharField(max_length=60, unique=True)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=120)
    is_active = models.BooleanField(default=True)
    requires_new_password = models.BooleanField(default=False)
    time_created = models.DateTimeField(auto_now_add=True)

    def set_password(self, password):
        import bcrypt
        self.password = bcrypt.hashpw(password.encode('utf-8'),
                                      bcrypt.gensalt(settings.BCRYPT_COST)).decode('utf-8')


class RememberMeToken(models.Model):
    token = models.CharField(max_length=60, primary_key=True)
    user = models.ForeignKey('User', on_delete=models.CASCADE)
