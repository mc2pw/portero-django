from django import forms
from django.contrib import admin

from .models import User

class UserAdminForm(forms.ModelForm):
    new_password = forms.CharField(widget=forms.PasswordInput, required=False)

    def save(self, commit=True):
        user = super().save(commit=False)
        new_password = self.cleaned_data["new_password"]
        if new_password:
            user.set_password(new_password)

        if commit:
            user.save()

        return user

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    readonly_fields = ['password']
    form = UserAdminForm
