from django.apps import AppConfig


class PorteroConfig(AppConfig):
    name = 'portero'
